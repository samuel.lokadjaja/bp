import geopy
import geopy.distance
import numpy as np
import random
from db import row_insert_query
import psycopg2.extensions
import psycopg2
from mission_status import *
from collections import deque
import time
from constants import *
import pyproj


class UAV:
    """The UAV class models a hybrid drone with one pusher motor, in constants defined number of hover motors and a battery."""
    # constants taken from the config file
    CRUISE_SPEED = CRUISE_SPEED  # in kmh
    TAKEOFF_TIME = TAKEOFF_TIME  # in minutes
    LANDING_TIME = LANDING_TIME  # in minutes
    BATTERY_DEGRADATION_FACTOR = BATTERY_CAPACITY_DEGRADATION_FACTOR  # decrement of battery capacity per mission
    N_HOVER_MOTORS = N_HOVER_MOTORS  # number of hover motors

    def __init__(self, uav_id: int):
        # there is an array of values that hold the information relating to the drone. Commented are the positions of the values in the array
        self.uav_id = uav_id  # 0
        self.flight_mode = FlightMode.NONE  # 1: type of flight mode, NONE when on ground
        self.hover_health = np.zeros(self.N_HOVER_MOTORS)  # [2:10]: health values of the hover motor
        self.pusher_health = None  # 10: health value of the pusher motor
        self.battery_level = None  # 11
        self.battery_capacity = 1  # maximum capacity of the battery, 100% at the beginning, decreases each mission
        self.hover_failure_appearance = np.empty(self.N_HOVER_MOTORS)  # [12:20]: threshold for faster degradation of hover motors
        self.pusher_failure_appearance = None  # 20: threshold for faster degradation of the hover motor
        self.n_missions = 0  # 21: number of finished missions
        self.mission_mode = MissionMode.AVAILABLE  # 22: current status of the UAV
        self.mission_progress = 0  # 23: progress of mission in %
        self.mission_type = MissionType.NONE  # 25: type of Mission
        self.health_initialization()  # initializing health values
        self.logs = deque(maxlen=LOGS_LEN)  # queue of mission log entries
        self.position = random.choice(list(BASES.values()))[1]  # 26, 27: position in (lat, long), at program start UAV's are distributed randomly among the available bases

        # parameters for SAR spiral search pattern
        self.angle = 65
        self.angle_diff = 0
        self.searching = False  # True if uav is searching during a SAR mission


    def set_log(self, log: MissionLog):
        """Writes the given value along with the time into the log queue

        Args:
            log(MissionLog): the value to be written in the log

        Returns: None
        """
        self.logs.appendleft({'s': self.log_to_text(log, self.mission_type), 't': time.strftime("%H:%M:%S", time.localtime()), 'l': str(log)})


    @staticmethod
    def log_to_text(log: MissionLog, mission_type: MissionType) -> str:
        """Generates a string for a log entry of type MissionLog and returns it

        Args:
            mission_type(MissionType): the mission type
            log(MissionLog): the given log entry

        Returns:
            str: String that contains the log entry
        """
        log_str = ""
        if log is MissionLog.MISSION_START:
            log_str = "UAV on {} Mission".format("Delivery" if mission_type == MissionType.DELIVERY_MISSION else "SAR")
        elif log is MissionLog.BATTERY_CRITICAL:
            log_str = "Battery critical"
        elif log is MissionLog.R2H_MISSION:
            log_str = "UAV returning to home"
        elif log is MissionLog.SAFELY_LANDED:
            log_str = "UAV has safely landed"
        elif log is MissionLog.BATTERY_CHARGING:
            log_str = "Battery is charging"
        elif log is MissionLog.BATTERY_CHARGED:
            log_str = "Battery fully charged"
        elif log is MissionLog.BATTERY_DEAD:
            log_str = "Battery dead"
        elif log is MissionLog.MISSION_COMPLETED:
            log_str = "Mission completed"

        return log_str


    def health_initialization(self) -> None:
        """Initializes the health values of the motors, the failure appearance, and the battery charge.
        Values are chosen randomly within a range depending on constants set inside the config file.

        Args: None

        Returns: None

        """
        # initial hover health
        for i in range(self.N_HOVER_MOTORS):
            self.hover_health[i] = random.uniform(*R_HOVER_HEALTH)
        # initial pusher health
        self.pusher_health = random.uniform(*R_PUSHER_HEALTH)
        # initial battery health = 100%
        self.battery_level = 1

        for i in range(self.N_HOVER_MOTORS):
            self.hover_failure_appearance[i] = round(random.uniform(*R_HOVER_FA), 3)

        self.pusher_failure_appearance = round(random.uniform(*R_PUSHER_FA), 3)


    def health_index(self) -> float:  # 26
        """Calculates the minimum of the motor health values

        Args: None

        Returns:
            float: minimum health value

        """
        return min([*self.hover_health, self.pusher_health])


    def store_to_database(self, con: psycopg2.extensions.connection, cur: psycopg2.extensions.cursor) -> None:
        """Writes the current drone values in the corresponding database tables

        Args:
            con(psycopg2.extensions.connection): Connection to database
            cur(): Database cursor

        Returns: None

        """
        cur.execute(row_insert_query.format(self.uav_id), self.get_array())  # execute the query
        con.commit()  # commit changes

    def get_array(self) -> np.ndarray:
        """Returns all drone kpi's as a numpy.array
        Args: None

        Returns:
            np.array: drone values
        """
        values = np.array(
            [self.uav_id,
             self.flight_mode.value,
             *self.hover_health,
             self.pusher_health,
             self.battery_level,
             *self.hover_failure_appearance,
             self.pusher_failure_appearance,
             self.n_missions,
             self.mission_mode.value,
             self.mission_progress,
             self.health_index(),
             self.mission_type.value,
             *self.position]
        )
        return values


    def degradation(self, flight_mode: int) -> None:
        """Simulates the motor degradation and battery discharge rate in relation to the current flight mode

        Args:
            flight_mode(int): Current flight mode -> 0 = Hover Mode, 1 = Cruise Mode

        Returns: None

        """
        self.flight_mode = FlightMode(flight_mode)

        if self.flight_mode == FlightMode.CRUISE_MODE:
            """Battery discharge:
                lower discharge rate, more constant discharge due to cruise mode"""
            self.battery_level -= round(random.uniform(*R_CRUISE_MODE_BATTERY_DISCHARGE), 3)  # discharge_rate

            """Pusher degradation:"""
            if self.pusher_health > self.pusher_failure_appearance:
                self.pusher_health -= round(random.uniform(*R_PUSHER_DEGRADATION_ABOVE_FA), 5)  # pusher_deg_rate above failure appearance
            else:
                self.pusher_health -= round(random.uniform(*R_PUSHER_DEGRADATION_BELOW_FA), 5)  # pusher_deg_rate below failure appearance
                
        else:
            """Battery discharge: much higher discharge rate, more rapid changes in discharge due to hover mode"""
            self.battery_level -= round(random.uniform(*R_HOVER_MODE_BATTERY_DISCHARGE), 2)  # discharge_rate

            """Hover degradation: generating a degeneration value for each hover motor"""
            hover_deg_values = np.empty(self.N_HOVER_MOTORS)
            for j in range(self.N_HOVER_MOTORS):
                if self.hover_health[j] > self.hover_failure_appearance[j]:
                    hover_deg_values[j] = round(random.uniform(*R_HOVER_DEGRADATION_ABOVE_FA), 4)  # hover_deg_rate above failure appearance
                else:
                    hover_deg_values[j] = round(random.uniform(*R_HOVER_DEGRADATION_BELOW_FA), 4)  # hover_deg_rate below failure appearance

            self.hover_health -= hover_deg_values

    def update_position(self, dest):
        """Updates the location of the drone using its mission type

        Args:
            dest(list[float]): destination of the drone in latitude and longitude

        Returns: None
        """
        geodesic = pyproj.Geod(ellps='WGS84')

        if self.searching and self.mission_type == MissionType.RECONNAISSANCE_MISSION:  # spiral pattern if uav is currently searching
            origin = geopy.Point(*self.position)
            if self.position == dest:  # first step is in northern direction
                #  computes the next position based on the current position, distance and angle (degrees)
                destination = geopy.distance.geodesic(kilometers=self.CRUISE_SPEED / 60).destination(origin, 0)
            else:   # followed by a spiral search with increasing angles between consecutive lines
                destination = geopy.distance.geodesic(kilometers=self.CRUISE_SPEED / 60).destination(origin, self.angle)

            self.position = [destination.latitude, destination.longitude]
            self.angle += 50 + self.angle_diff  # angle is increased every iteration
            self.angle_diff -= 1  # with a decreasing difference

        else:  # normal uav movement
            if geopy.distance.distance(self.position, dest).meters < self.CRUISE_SPEED / 60 * 1000:  # if distance to destination < travel distance in one minute
                if self.mission_type == MissionType.RECONNAISSANCE_MISSION:
                    self.searching = True
                self.position = dest  # uav reached destination
                return

            brng = geodesic.inv(*self.position[::-1], *dest[::-1])[0]  # angle between start and destination. Somehow the method requires the order longitude, latitude

            origin = geopy.Point(*self.position)
            destination = geopy.distance.geodesic(kilometers=self.CRUISE_SPEED/60).destination(origin, brng)

            self.position = [destination.latitude, destination.longitude]

    def get_base(self):  # currently not used but might be useful later on
        """Returns the base in which the drone is located and None if it's not located at any

        Args: None

        Returns: 
            str: the base the drone is located in
        """
        for base in BASES:
            if self.position == BASES[base][1]:
                return base
        else:
            return None
