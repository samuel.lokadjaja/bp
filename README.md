# Asynchrones und Eventbasiertes Dashboard
This project serves as a dashboard to overview multiple drones. On the right side of the dashboard there is a control center, which can be used to choose drones and send them to a chosen mission. On the left side of the dashboard there is a tab that contains an overview on the drone's KPI (Key Performance Index) and a tab that functions as a map.

The **Overview** tab shows the status of each drone and also has a **Details** button that allows users to see the graphs and states of the drones: Battery Health, Motor Healths. 
![](data/dashboard1.png)

The **Map** tab allows users to set different start and end points to send drones on missions. The different missions are *Delivery and Search-and-Rescue (SAR)*. On a *delivery mission*, a drone sets off from the starting point and flies to the destination. The default maximum flight distance is set to 60 kilometers. On a *SAR mission*, a square is drawn on the map, and the drone flies to it from a starting location. The default maximum flight distance is set to 15 kilometers. For both mission types, drones can be sent individually or in a fleet. 
![](data/dashboard2.png)

# Getting Started
There are multiple packages and programs that have to be installed before the dashboard can function. The following sections provide information on what they are and how to install them.

## Python
The project was written in Python3. To download python, follow the instructions found [here](https://www.python.org/downloads/). This link goes to the python download page and can provide instructions for the installation.

### The Environment (Optional)
A python environment is an way to install python libraries, scripts, and interpreters in closed areas that are not directly in the operating system.

If this is useful or necessary, then the following steps will help create an environment to use.
The environment will be created where the project is located, so make sure to take note of where the project is downloaded. 

To create the environment, first go to the project location in the terminal using the `cd` command. Once there, the following command will create a **v**irtual **env**ironment (venv). Make sure to change the path/to/project to the location of the project.

    python3 -m venv <path/to/project>

Once that has been created, the environment has to be activated. To activate the venv, go to the location of the virtual environment, and then execute the `\Scripts\activate.bat` file. Once done, `deactivate` will "turn off" the environment.

### The Python Libraries
There are multiple python libraries used for this project. These can be found in the *requirements.txt* file. 

To install the libraries, **pip** will be used as a manager:

- To install **pip** use the following command:

      py -m ensurepip --upgrade 

Once both python and pip are installed, the libraries can be imported. Use the following command:

    pip install -r requirements.txt 

This will install all the required libraries.

## The Database
The database used is **PostgreSQL**. To download **PostgreSQL**, follow the instructions found [here](https://www.postgresql.org/download/). This link goes to the **PostgreSQL** download page and can provide instructions for the installation.

### Passwords
During the installation, it is important to install it with the proper password.
*Important*: The PostgreSQL **USER** password is the one that must be set to the same one in the project.
#### Master Password
There is a master password, that allows access to the pgadmin4 application. If the master password is forgotten, a reset is possible. The reset will eliminate all settings and servers saved.
#### User Password
The user password is the password that will allow access into the database itself. It is important that the log in credentials are the same in the project and the database. The user password can be found and edited within the config file. This way there is no security vulnerability in the sense of hardcoded passwords within the application.

 
### Database Servers
In order for the data to be stored, a database needs to be created. Within the pgadmin4 application, create a new database named *av_health*

## The Dashboard
In order to access the dashboard, the correct python file has to be executed. Use the following command: 

    py ./Dashboard.py

This will start the dashboard and allow you to interact with it. In order to view the dashboard, open the browser of your choice and enter `https://127.0.0.1:8050` as your website. Now the dashboard will be available to view and interact with.

### PyCharm
We recommend to use an IDE (integrated development environment) **PyCharm** to run the dashboard python program. Follow the instructions found [here](https://www.jetbrains.com/pycharm/download/). This link goes to the **PyCharm** download page and can provide instructions for the installation.

### Chrome Browser
We recommend to use **Google Chrome Browser** to view and interact with the dashboard. Follow the instructions found [here](https://www.google.com/chrome/). This link goes to the **Google Chrome Browser** download page and can provide instructions for the installation.

## Configuration File
There is a configuration file that allows for users to set multiple custom variables and parameters within the project. A few examples of these parameters are the number of drones, base locations, and maximum flight distance, among others. If no values are given within the file, then default values are used. However, there is no check for improper values, such as negative distance or negative amount of drones. 

# License
MIT License

Copyright (c) 2022 

Lorenz Dingeldein,
Arne Schuler,
Daniel Jonatan,
Patrick Paysen,
Raynard Widjaja,
Samuel Lokadjaja


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
