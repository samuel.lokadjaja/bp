import psycopg2
import psycopg2.extensions
from constants import N_HOVER_MOTORS, HOST, DATABASE, USER, PASSWORD

"""This script handles connection to the database. """

#  4 strings with variable length depending on the number of hover motors
hv_str = ""
hv_str_c = ""
hv_fa_str = ""
hv_fa_str_c = ""

for i in range(N_HOVER_MOTORS):
    hv_str += f"hv{i+1}, "
    hv_str_c += f"hv{i+1} real, "
    hv_fa_str += f"hv{i+1}_fa, "
    hv_fa_str_c += f"hv{i+1}_fa real, "

row_insert_query = "insert into uav{} (uav_id, flight_mode, " + hv_str + "psh, bat_h, " \
                   + hv_fa_str + "pusher_fa, no_missions, mission_mode, mission_progress," \
                   " health_index, mission_type, pos_long, pos_lat) values (" + 2*N_HOVER_MOTORS*"%s," + "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

create_table_query = "create table if not exists uav{} (index serial primary key, uav_id integer, flight_mode real, " + hv_str_c +\
                     "psh real, bat_h real, " + hv_fa_str_c + "pusher_fa real, no_missions integer,  mission_mode real, mission_progress real," \
                     " health_index real, mission_type real, pos_long real, pos_lat real);"


def init_database(uav_count: int) -> psycopg2.extensions.connection:
    """creates a table for each UAV in the database
    Args:
        uav_count(int): the number of drone

    Returns:
        psycopg2.extensions.connection: connection to database

    """
    # connect to postgres
    con = psycopg2.connect(
        host=HOST,
        database=DATABASE,
        user=USER,
        password=PASSWORD
    )
    # cursor
    cur = con.cursor()

    for u in range(uav_count):
        # delete already existing tables
        cur.execute(f"drop table if exists uav{u+1}")
        # create empty uav table
        cur.execute(create_table_query.format(u+1))

    con.commit()
    cur.close()

    return con
