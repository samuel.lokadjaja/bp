import configparser
import json

'''
This file parses the config file and stores it's values in corresponding constants. By providing fallback values for each constant it is possible
to delete currently unimportant parameters from the file, they are then replaced with default values.

The variables have been divided in different categories to make it easier to identify what their functionality is.

TIME - the time variables involve the scaling of system time to real time
UAV - the UAV variables allow a user to make changes involved with the UAVs: the number of UAVs and motors, speed of flight in different modes, etc.
DATABASE - the database variables allow a user to define the connection to the database and its credentials
MISSION - the mission variables allow a user to change settings involved with mission execution, status, or generation: mission distance, preparation time, bases, etc.
DASHBOARD - the dashboard variables allow a user to change the way the dashboard operates: Log length, dashboard update interval
SIMULATION - the simulation variables allow a user to set different variables for the simulation
'''

config = configparser.ConfigParser()
config.sections()
config.read('config.ini')


# [TIME]
TIME_SCALE = config.getfloat("TIME", "timescale", fallback=1.0)

# [UAV]
N_UAVS = config.getint("UAV", "n_uavs", fallback=50)
CRUISE_SPEED = config.getint("UAV", "cruise_speed", fallback=60)
TAKEOFF_TIME = config.getint("UAV", "takeoff_time", fallback=2)
LANDING_TIME = config.getint("UAV", "landing_time", fallback=2)
N_HOVER_MOTORS = config.getint("UAV", "n_hover_motors", fallback=8)
SEARCH_RANGE = config.getfloat("UAV", "search_range", fallback=1.85201)


# [Database]
HOST = config.get("Database", "host", fallback="localhost")
DATABASE = config.get("Database", "database", fallback="av_health")
USER = config.get("Database", "user", fallback="postgres")
PASSWORD = config.get("Database", "password", fallback="R!3senfelge")

# [Mission]
MAX_OP_TIME = config.getint("Mission", "max_op_time", fallback=50)
MISSION_PREP_TIME = config.getint("Mission", "mission_prep_time", fallback=5)
BATTERY_CHARGING_TIME = config.getint("Mission", "battery_charging_time", fallback=10)
DEL_MISSION_MAX_DIST = config.getint("Mission", "del_mission_max_dist", fallback=60)
REC_MISSION_MAX_DIST = config.getint("Mission", "rec_mission_max_dist", fallback=15)
R_SAR_HOVER_TIME = json.loads(config.get("Mission", "r_sar_hover_time", fallback="[1, 5]"))
R_SAR_CRUISE_TIME = json.loads(config.get("Mission", "r_sar_cruise_time", fallback="[1, 3]"))
BASES = json.loads(config.get("Mission", "bases", fallback="""{
    "Wangerooge": [8, [53.776313, 7.868275]],
    "Helgoland": [14, [54.175782, 7.89247]],
    "Cuxhafen": [15, [53.876664, 8.698272]],
    "Buesum": [17, [54.122698, 8.858142]],
    "Eiderdamm": [18, [54.265837, 8.847793]],
    "Amrum": [20, [54.631605, 8.380171]],
    "Hoernum": [21, [54.759412, 8.295176]],
    "Nordholz": [23, [53.767222, 8.658611]]
    }"""))

# [Dashboard]
UPDATE_INTERVAL = config.getint("Dashboard", "update_interval", fallback=1000)
LOGS_LEN = config.getint("Dashboard", "logs_len", fallback=10)

# [Simulation]
BATTERY_CAPACITY_DEGRADATION_FACTOR = config.getfloat("Simulation", "battery_capacity_degradation_factor", fallback=0.0002)
R_HOVER_HEALTH = json.loads(config.get("Simulation", "hover_health", fallback="[0.95, 1.0]"))
R_PUSHER_HEALTH = json.loads(config.get("Simulation", "pusher_health", fallback="[0.95, 1.0]"))
R_HOVER_FA = json.loads(config.get("Simulation", "r_hover_fa", fallback="[0.4, 0.55]"))
R_PUSHER_FA = json.loads(config.get("Simulation", "r_pusher_fa", fallback="[0.4, 0.5]"))
R_HOVER_MODE_BATTERY_DISCHARGE = json.loads(config.get("Simulation",  "r_hover_mode_battery_discharge", fallback="[0.03, 0.07]"))
R_CRUISE_MODE_BATTERY_DISCHARGE = json.loads(config.get("Simulation", "r_cruise_mode_battery_discharge", fallback="[0.005, 0.015]"))
R_PUSHER_DEGRADATION_ABOVE_FA = json.loads(config.get("Simulation", "r_pusher_degradation_above_fa", fallback="[0.000046, 0.000092]"))
R_PUSHER_DEGRADATION_BELOW_FA = json.loads(config.get("Simulation", "r_pusher_degradation_below_fa", fallback="[0.00011, 0.000295]"))
R_HOVER_DEGRADATION_ABOVE_FA = json.loads(config.get("Simulation", "r_hover_degradation_above_fa", fallback="[0.0002, 0.0004]"))
R_HOVER_DEGRADATION_BELOW_FA = json.loads(config.get("Simulation", "r_hover_degradation_below_fa", fallback="[0.0003, 0.001]"))
