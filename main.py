from mission_status import MissionType
import matplotlib.pyplot as plt
from mission_execution import mission_execution
from uav import UAV
import random
import pandas as pd
from pathlib import Path
from db import init_database

"""
This script used to be the main script of the project before the dashboard was implemented. It's not executable at the moment
due to dependencies on parts of the project that have been changed since. It might be useful to revive it for debug purposes,
e.g. simulating missions without starting the dashboard
"""


len_uav_array = 28
NUMBER_OF_UAVS = 10
no_ex_missions = 1  # number of missions per UAV


def main(show_graphs=True) -> None:
    """former main method of the project:
    At first a connection to the database is set up, then missions for each UAV are executed inside a loop,
     followed by plotting the values."""

    # generates the data folder, if not existing
    Path("data").mkdir(parents=True, exist_ok=True)

    con = init_database(NUMBER_OF_UAVS)
    # cursor
    cur = con.cursor()

    for i in range(NUMBER_OF_UAVS):
        uav = UAV(i+1)

        # insert initial health into database
        uav.store_to_database(con, cur)

        uav.mission_type = random.choice([MissionType.DELIVERY_MISSION, MissionType.RECONNAISSANCE_MISSION])

        for j in range(no_ex_missions):
            mission_length = random.randint(*[40, 60] if uav.mission_type == MissionType.DELIVERY_MISSION else [5, 15])
            print('No. of Mission: ', j, '\t\tMission Type: ', uav.mission_type.name, '\t\tLength: ', mission_length)
            mission_execution(uav, mission_length, con)

        cur.execute(f'select * from uav{uav.uav_id}')
        uav_complete = pd.DataFrame(cur.fetchall())
        uav_complete.to_csv(f'data/complete_health_uav{uav.uav_id}.csv', sep=",", header=False)

        if show_graphs:
            col_list = ['C0', 'C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7']
            for j in range(len(uav.hover_health)):
                plt.plot(uav_complete.iloc[:, 3 + j], color=col_list[j], label=f"Hover {j} health")
                plt.plot(uav_complete.iloc[:, 14 + j], '--', color=col_list[j],
                         label=f"Failure threshold for hover {j}")

            # plt.plot(uav_complete.iloc[:, 2], 'k')
            plt.plot(uav_complete.iloc[:, 11], 'k', label="Pusher Health")
            plt.plot(uav_complete.iloc[:, 12], label="Battery capacity")
            # plt.plot(uav_complete.iloc[:, 13], 'k--', label="Failure Threshold for pusher")
            # plt.plot(uav_complete.iloc[:, 27], 'k.')
            plt.legend(loc="lower left")
            plt.xlabel("Time in min")
            plt.ylabel("Health grade")
            plt.grid(True)
            plt.title("UAV Degradation")
            plt.show()

    # close cursor
    cur.close()
    # close database
    con.close()



if __name__ == '__main__':
    main()
