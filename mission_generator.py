import numpy as np
from uav import UAV
import math
from mission_status import *
from constants import MAX_OP_TIME

# AV performance data
max_op_time = MAX_OP_TIME  # in minutes

# two loadcases:
# cruisemode = 0
# hovermode = 1

# generating mission profiles


def mission_profile(distance: float, mission_type: MissionType) -> np.ndarray:
    """Creates a mission profile to the corresponding mission in an array format. The value at index i of the array stands
    for the flight mode at minute i during the mission.

    Args:
        mission_type(MissionType): The type of the mission to generate a profile for
        distance(int): The distance to the destination of the mission

    Returns:
        np.ndarray: An array, whose index i describes the flight mode of the minute i

    """
    approaching_time = math.ceil(distance / UAV.CRUISE_SPEED * 60)  # time to reach to destination, in min, rounded up

    if mission_type == MissionType.DELIVERY_MISSION:
        profile = np.zeros(UAV.TAKEOFF_TIME + approaching_time + UAV.LANDING_TIME)
        profile[UAV.TAKEOFF_TIME:-UAV.LANDING_TIME] = FlightMode.CRUISE_MODE.value  # cruising
    elif mission_type == MissionType.R2H_MISSION:
        profile = np.zeros(approaching_time + UAV.LANDING_TIME)
        profile[:approaching_time] = FlightMode.CRUISE_MODE.value
    else:  # SAR mission
        profile = np.zeros(max_op_time)
        reconnaissance_time = max_op_time - (UAV.TAKEOFF_TIME + 2 * approaching_time + UAV.LANDING_TIME)  # time available for searching
        rec_base_time = UAV.TAKEOFF_TIME + approaching_time

        profile[rec_base_time:rec_base_time + reconnaissance_time] = FlightMode.CRUISE_MODE.value  # searching in cruise mode
        
        profile[UAV.TAKEOFF_TIME:UAV.TAKEOFF_TIME + approaching_time] = FlightMode.CRUISE_MODE.value  # transfer to designated area
        profile[-(UAV.LANDING_TIME + approaching_time):-UAV.LANDING_TIME] = FlightMode.CRUISE_MODE.value  # transfer from designated area

    # takeoff and landing in hover mode
    if mission_type != MissionType.R2H_MISSION:  # no takeoff for r2h mission
        profile[:UAV.TAKEOFF_TIME] = FlightMode.HOVER_MODE.value  # takeoff
    profile[-UAV.LANDING_TIME:] = FlightMode.HOVER_MODE.value  # landing

    return profile
