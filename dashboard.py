from functools import partial
import geopy.distance
import plotly.graph_objects as go
from dash import Dash, dcc, html, Input, Output, State
from dash.exceptions import PreventUpdate
import dash_bootstrap_components as dbc
from uav import UAV
import dash
import threading
from db import init_database
from mission_status import *
from mission_execution import mission_execution
from dash import dash_table as dt
import dash_leaflet as dl
from constants import BASES, N_UAVS, UPDATE_INTERVAL, DEL_MISSION_MAX_DIST, REC_MISSION_MAX_DIST, SEARCH_RANGE
import dash_daq as daq

""" see here https://dash.plotly.com/ for documentation about plotly dash and its components """

start_pos = [54.3, 8.3]  # North Sea

app = Dash(external_stylesheets=[dbc.themes.BOOTSTRAP], assets_folder="data")

# set up a connection
con = init_database(N_UAVS)

# initialize each UAV
UAVS = []
for i in range(N_UAVS):
    uav = UAV(i + 1)
    UAVS.append(uav)
    uav.store_to_database(con, con.cursor())

# KPI graph for each UAV
fig = []
for i, uav in enumerate(UAVS):
    fi = go.Figure(go.Bar(
        x=[uav.battery_level, uav.pusher_health, *uav.hover_health[::-1]],
        y=['Battery', 'Pusher Motor'] + [f"Hover Motor {i}" for i in range(UAV.N_HOVER_MOTORS, 0, -1)],
        orientation='h'))
    fi.update_layout(title={'text': "Health", 'y': 1, 'x': 0.5, 'xanchor': 'center', 'yanchor': 'top'},
                     width=320, height=200, margin=dict(l=10, r=0, b=0, t=20, pad=2), )
    fig.append(fi)

# control menu on the left side of the dashboard
left = dbc.Form([
    # select uavs
    dbc.Label("UAV Selection", style={'marginTop': '1.5vw'}),
    dcc.Dropdown(id="Select UAV", multi=True, placeholder="Select a UAV", value=[]),

    # select mission type
    dbc.Label("Mission Selection", width=10, style={'marginTop': '2vw'}),
    dbc.RadioItems(
        id="mission",
        options=[
            {"label": "Delivery Mission", "value": MissionType.DELIVERY_MISSION.value},
            {"label": "SAR Mission", "value": MissionType.RECONNAISSANCE_MISSION.value},
        ],
        value=MissionType.DELIVERY_MISSION.value,
    ),

    dbc.Row(id="row_mission_start", children=[
        dbc.Col([
            dbc.Label("Start Base", style={'marginTop': '1.5vw'}),
            dcc.Dropdown(id="select_mission_start", value=None,
                         options=[{"label": base, 'value': base, 'disabled': False} for i, base in enumerate(BASES)]),
        ]),
        dbc.Col(id="row_mission_dest", children=[
            dbc.Label("Destination Base", style={'marginTop': '1.5vw'}),
            dcc.Dropdown(id="select_mission_dest", value=None,
                         options=[{"label": base, 'value': base, 'disabled': False} for i, base in enumerate(BASES)]),
        ]),

    ]),

    # mission distance
    dbc.Label(f"Select a route",
              width=10,
              style={"marginTop": "1.5vw", "color": "red"},
              id="mission_length"),

    # run button (execute mission)
    dbc.Button("Start Mission", id="start", style={'marginTop': '0vw'}, disabled=True),

    # sort uav
    dbc.Row(id="row_sort", children=[
        dbc.Label("Sort UAVS", style={'marginTop': '1.5vw'}),
        dcc.Dropdown(id="sort_uavs", multi=False, placeholder="Select Sort Type",
                     options=[
                         {"label": "ID ascending", 'value': 0, 'disabled': True},
                         {"label": "ID descending", 'value': 1, 'disabled': False},
                         {"label": "Overall Health ascending", 'value': 2, 'disabled': False},
                         {"label": "Overall Health descending", 'value': 3, 'disabled': False},
                         {"label": "Mission Status: ON_MISSION", 'value': 4, 'disabled': False},
                         {"label": "Mission Status: AVAILABLE", 'value': 5, 'disabled': False}
                     ], optionHeight=26),

        dbc.Button("Sort", id='sort', style={'verticalAlign': 'middle', 'marginTop': '1.5vw', "width": "60px"},
                   disabled=True)
    ], style={"display": "block"}),
])

# status of each UAV in the overview tab
uav_stats = []

for i in range(N_UAVS):
    uav_stats.append(
        dbc.Row(children=[
            # each Col represents a column inside the tab
            dbc.Col(children=[
                html.Label(f"UAV {i + 1}",
                           style={'verticalAlign': 'middle', 'textAlign': 'center', 'fontSize': '32px'}),

                daq.BooleanSwitch(id=f'select_switch{i + 1}',
                                  on=False,
                                  style={'marginTop': '0.5vw'},
                                  color="#77b470",
                                  disabled=True,
                                  theme="dark"),
            ]),
            dbc.Col(children=[
                html.Label("Overall Health", style={'verticalAlign': 'middle', 'textAlign': 'center'}),
                html.Br(),
                html.Label("INITIALIZING", id=f"ovr_health{i + 1}",
                           style={'verticalAlign': 'middle', 'textAlign': 'center'}),
            ]),
            dbc.Col(children=[
                html.Label("Mission Progress", style={'verticalAlign': 'middle', 'textAlign': 'center'}),
                html.Br(),
                dbc.Progress(value=0, id=f"mission_progress{i + 1}", animated=True, striped=True)
            ], width=4),
            dbc.Col(children=[
                html.Label("Mission Status", style={'verticalAlign': 'middle', 'textAlign': 'center'}),
                html.Br(),
                html.Label("INITIALIZING", id=f"mission_status{i + 1}",
                           style={'verticalAlign': 'middle', 'textAlign': 'center'})
            ]),
            dbc.Col(children=[
                dbc.Button("Details", id=f'detail{i + 1}', size='lg',
                           style={'verticalAlign': 'middle', 'textAlign': 'center'})
            ]),

            # collapse to show further details
            dbc.Collapse(
                dbc.Card(dbc.CardBody([
                    dbc.Row([
                        dbc.Col(children=[
                            dcc.Graph(id=f"kpis{i + 1}", figure=fig[i])
                        ], style={}),
                        dbc.Col(children=[
                            html.Label("Most critical subsystem",
                                       style={'verticalAlign': 'middle', 'textAlign': 'center'}),
                            html.Br(),
                            html.Label("Hover motor ", style={'verticalAlign': 'middle', 'textAlign': 'center'}),
                            html.Br(),
                            html.Br(),
                            html.Label("Mission success rate",
                                       style={'verticalAlign': 'middle', 'textAlign': 'center'}),
                            html.Br(),
                            html.Label("98%", style={'verticalAlign': 'middle', 'textAlign': 'center'}),
                            html.Br(),
                            html.Br(),
                            html.Label("RUL", style={'verticalAlign': 'middle', 'textAlign': 'center'}),
                            html.Br(),
                            html.Label("296h", style={'verticalAlign': 'middle', 'textAlign': 'center'})
                        ], style={}),
                        dbc.Col(children=[
                            dt.DataTable(id=f"tbl{i + 1}",
                                         data=list(UAVS[i].logs),
                                         columns=[{'name': 'time', 'id': 't'}, {'name': 'logs', 'id': 's'}],
                                         style_cell={'textAlign': 'left'},
                                         style_table={'height': '220px', 'overflowY': 'auto', 'overflowX': 'hidden',
                                                      'width': '320px'},
                                         page_action='none'
                                         )
                        ], style={}),
                    ], className="g-0")
                ])),
                id=f"collapse{i + 1}",
                is_open=False,
            ),

        ], style={'display': 'flex', 'margin': "auto"},
            id=f"row{i + 1}",
            key=f"{i + 1}",
            className="border rounded"))

# the right side of the dashboard that contains the overview and map tabs
right = dbc.Form([
    dcc.Tabs(id="tabs", value="uavs", children=[
        # overview tab
        dcc.Tab(value="uavs", label="Overview",
                id="uebersicht",
                children=uav_stats),

        # map tab
        dcc.Tab(value="map", label="Map", children=[
            html.Div([
                dl.Map(children=[
                    dl.TileLayer(),

                    # edit control for drawing SAR rectangles
                    dl.FeatureGroup(
                        id='edit',
                        children=[dl.EditControl(
                            id="edit_control",
                            draw=dict(circle=False, rectangle=False, circlemarker=False,
                                      marker=False, polyline=False, polygon=False))]),

                    # wrappers for start and destination markers (workaround for hiding markers)
                    dl.LayerGroup(id="start_group"),
                    dl.LayerGroup(id="dest_group"),

                    # wrapper for all uavs on the map
                    dl.LayerGroup(id="uav_fleet"),

                    # wrapper for bases and route
                    dl.LayerGroup([
                        *[dl.Marker(icon={"iconUrl": "http://127.0.0.1:8050/assets/base.png", "iconAnchor": [16, 16]},
                                    id=f"base_{b}", position=BASES[b][1],
                                    children=dl.Tooltip(f"Base {BASES[b][0]}: " + b),
                                    bubblingMouseEvents=True)
                          for b in BASES],
                        dl.PolylineDecorator(
                            id="route",
                            positions=[],
                            patterns=[dict(repeat='10',
                                           dash=dict(pixelSize=5,
                                                     pathOptions=dict(color='#000',
                                                                      weight=2,
                                                                      opacity=0.5)))]),
                    ], id="layer")],
                    id="map", style={'width': '100%', 'height': '79vh', 'margin': "auto", "display": "block"},
                    center=start_pos, zoom=8, ),
                html.Br(),

                html.Div(
                    [dbc.RadioItems(
                        id="start_dest_radio",
                        className="btn-group",
                        inputClassName="btn-check",
                        labelClassName="btn btn-outline-primary",
                        labelCheckedClassName="active",
                        options=[
                            {"label": "Start", "value": 1},
                            {"label": "Destination", "value": 2},
                        ],
                        value=1,
                    ),
                        html.Div(id="output"),
                    ],
                    className="radio-group",
                ),
            ])
        ]),
    ], style={"marginTop": "1vw", "position": "sticky", "top": "0"})
])

# the whole dashboard (combination of left [Line 43] and right [Line 182] )
app.layout = html.Div([
    dcc.Interval(id="progress_interval", interval=UPDATE_INTERVAL),
    dcc.Interval(id="map_interval", interval=UPDATE_INTERVAL),
    dbc.Container(fluid=True, children=[
        dbc.Row([
            dbc.Col(width=3, children=[
                html.Div(children=[
                    html.H3(children='UAV Health Dashboard', style={'marginTop': '0.5vw'}),
                    left
                ], style={"padding": "20px", 'verticalAlign': 'middle', 'textAlign': 'center', 'position': 'sticky', "backgroundColor": "#fefefe",
                          'top': '0', "overflowY": "scroll", "height": "100vh"}, className="border rounded"),
                html.Br()
            ]),
            dbc.Col(width=9, children=[
                html.Div(children=[
                    right
                ], style={'verticalAlign': 'middle', 'textAlign': 'center', "backgroundColor": "#fefefe"}, className="border rounded")
            ]),
        ]),
    ])], style={"backgroundColor": "#ccd2cc"})

# --------------------- CALLBACKS ----------------------------------------------------------------------------------------------------
"""In plotly dash a callback is a function triggered by the dashboard itself to change the status of it's components.
Each callback takes values from components and outputs values to components. A callback fires if one or more of the input components has been changed"""


@app.callback(
    Output("Select UAV", "options"),
    [Output(f"select_switch{i + 1}", "disabled") for i in range(N_UAVS)],
    [Input("progress_interval", "n_intervals"),
     Input("map_interval", "n_intervals"),
     Input('select_mission_start', 'value')],
    prevent_initial_call=True
)
def update_selected_uav(progress_interval, map_interval, start_base):
    """
    disables non available uavs in the dropdown and the selected uavs in the uav tabs

    Args:
        progress_interval(int): n_intervals of the uav overview update-interval (value not used, only the event)
        map_interval(int): n_intervals of the map-interval (value not used, only the event)
        start_base(str): currently selected startbase from the dropdown

    Returns:
        dict: dictionary of options for the dropdown
        list[bool]: disables the switch at the corresponding index

    """
    trigger = dash.callback_context.triggered[0]["prop_id"]

    if start_base is None:
        if trigger == 'select_mission_start.value':
            return [], *[True for i in range(N_UAVS)]
        else:
            raise PreventUpdate

    options = []
    disabled = []

    for i, uav in enumerate(UAVS):
        if uav.position == BASES[start_base][1]:
            options.append(
                {"label": f"UAV {i + 1}", 'value': i + 1, 'disabled': uav.mission_mode != MissionMode.AVAILABLE})
        disabled.append(not uav.position == BASES[start_base][1] or uav.mission_mode != MissionMode.AVAILABLE)

    return options, *disabled


@app.callback(
    [Output("Select UAV", "value")]
    + [Output(f"select_switch{i + 1}", "on") for i in range(N_UAVS)],
    [Input("start", "n_clicks"),
     Input("Select UAV", 'value')]
    + [Input(f"select_switch{i + 1}", "on") for i in range(N_UAVS)],
    prevent_initial_call=True
)
def update_dropdown(n_clicks, dropdown_values, *switches):
    """
    maintains consistency between the selected uavs in the uav tabs and the dropdown and resets them, if the start button is pressed

    Args:
        n_clicks(int): n_clicks of the mission start button (value not used, only the event)
        dropdown_values(list[int]): id's of selected UAV's from the dropdown
        *switches(list[str]): list of booleans, true if switch at index i is on

    Returns:
        list[str]: the selected uavs for the dropdown
        list[bool]: sets the switch at the corresponding index to on

    """
    trigger = dash.callback_context.triggered[0]["prop_id"]

    if trigger == "start.n_clicks":  # resets both when the start button is pressed
        return [], *[False for i in range(N_UAVS)]
    elif trigger == "Select UAV.value":
        return dash.no_update, *[(i + 1 in dropdown_values) for i in range(N_UAVS)]
    else:
        selected = []
        for i, s in enumerate(switches):
            if s:
                selected.append(i + 1)
        return selected, *[dash.no_update for i in range(N_UAVS)]


@app.callback(
    Output("uav_fleet", "children"),
    Input("map_interval", "n_intervals")
)
def update_fleet(n_intervals):
    """
    draws the UAV's on mission onto the map. If UAV's have equal positions, only one is drawn and their id's are added to the tooltip.
    draws a circle with the UAV

    Args:
        n_intervals(int): n_intervals of the map update-interval (value not used, only the event)

    Returns:
        list[dl.Marker]: with an marker for each UAV

    """
    fleet = []
    positions = []
    uavs = []

    for uav in UAVS:
        if uav.mission_mode in [MissionMode.DELIVERY_MISSION, MissionMode.R2H_MISSION, MissionMode.RECONNAISSANCE_MISSION]:  # uav on mission
            if uav.position in positions:  # another uav has the same position
                uavs[positions.index(uav.position)].append(uav.uav_id)
            else:
                positions.append(uav.position)
                uavs.append([uav.uav_id])

    for i, pos in enumerate(positions):
        fleet.append(dl.Marker(
            children=dl.Tooltip("UAV {}".format(", ".join(map(str, uavs[i])))),
            icon={"iconUrl": "http://127.0.0.1:8050/assets/" + (
                "Hybrid_UAV_with_props.svg" if UAVS[uavs[i][0]].flight_mode == FlightMode.HOVER_MODE else "Hybrid_UAV_without_props.svg"),
                  "iconSize": [32, 32], "iconAnchor": [16, 16]},
            position=pos))
        for uav_id in uavs[i]:
            if UAVS[uav_id - 1].searching:
                fleet.append(dl.Circle(id="sar_circle", opacity=1, center=pos, radius=SEARCH_RANGE * 1000,
                                       color='rgb(128,128,0,0.4)'))
    return fleet


@app.callback(
    [Output("start", "disabled"),
     Output("route", "positions"),
     Output('mission_length', 'children'),
     Output('mission_length', 'style')],
    Input("Select UAV", "value"),
    Input('start_group', 'children'),
    Input('dest_group', 'children'),
    State("mission", "value"),
    prevent_initial_call=True
)
def update_route(selected_uavs, start_group, dest_group, mission_value):
    """
    sets button start on disabled, if no UAV or no route is currently selected. Otherwise it draws the route onto the map and updates components
    to show route information

    Args:
        selected_uavs(list[int]):
        start_group: list of dictionaries for each object inside start_group
        dest_group: list of dictionaries for each object inside dest_group
        mission_value: value of the selected mission type

    Returns:
        bool: True if the start button should be disabled
        list[list[int]]: containing start and destination coordinates for the route
        str: showing the length of the route. Stating "select a route" if there is none
        dict: containing the style for the route text: green if route is possible else red

    """
    if start_group == [] or dest_group == []:  # if either start or destination are missing
        return [True,
                [],
                "Select a route",
                {"marginTop": "1.5vw", "color": "red"}]
    else:
        start = [0, 0]
        for elem in start_group:
            if elem["props"]["id"] == "marker_start":
                start = elem["props"]["position"]  # position of the start marker
        dest = dest_group[0]["props"]["position"]  # position of the destination marker
        mission_type = MissionType(mission_value)
        max_distance = DEL_MISSION_MAX_DIST if mission_type == MissionType.DELIVERY_MISSION else REC_MISSION_MAX_DIST
        distance = geopy.distance.great_circle(start, dest)
        return [len(selected_uavs) == 0 or geopy.distance.great_circle(start, dest) > max_distance,
                [start, dest] if distance <= max_distance else [],
                f"Mission Length: {round(distance.km, 2)}km",
                {"marginTop": "1.5vw", "color": "green" if distance <= max_distance else "red"}]


@app.callback(
    Output('select_mission_start', 'options'),
    Input('select_mission_dest', 'value'),
    prevent_initial_call=True
)
def disable_start_base(dest_base):
    """
    disables the currently selected destination base inside the start base dropdown

    Args:
        dest_base(str): currently selected destination base, None if there is none

    Returns:
        dict: options for the dropdown, destination base is disabled

    """
    return [{"label": base, 'value': base, 'disabled': base == dest_base} for i, base in enumerate(BASES)]


@app.callback(
    Output('select_mission_dest', 'options'),
    Input('select_mission_start', 'value'),
    prevent_initial_call=True
)
def disable_dest_base(start_base):
    """
    disables the currently selected start base inside the destination base dropdown

    Args:
        start_base: currently selected start base, None if there is none

    Returns:
        dict: options for the dropdown, start base is disabled

    """
    return [{"label": base, 'value': base, 'disabled': base == start_base} for i, base in enumerate(BASES)]


@app.callback(
    Output('select_mission_start', 'value'),
    Output('select_mission_dest', 'value'),
    Input('map', 'click_lat_lng'),
    Input("mission", "value"),
    State('select_mission_start', 'value'),
    State('select_mission_dest', 'value'),
    State("start_dest_radio", "value"),
    prevent_initial_call=True
)
def base_select(click, mission_value, start_base, dest_base, start_dest_value):
    """
    checks if a base was clicked, if yes sets the start or destination base, depending on the radio-component.
    Also checks, if the base is already chosen.

    Args:
        click(list[int]): coordinates of the click event
        mission_value(int): value of the selected mission
        start_base(str): currently selected start base
        dest_base(str): currently selected destination base
        start_dest_value(int): 1 if start is selected, 2 if destination

    Returns:
        str: selected start base
        str: selected destination base

    """
    new_value_start = dash.no_update
    new_value_dest = dash.no_update
    trigger = dash.callback_context.triggered[0]["prop_id"]

    if trigger == "mission.value":
        return new_value_start, None
    else:
        for base in BASES:
            if BASES[base][1] == click:  # a base was clicked
                if start_dest_value == 1 and base != dest_base:
                    new_value_start = base
                elif MissionType(mission_value) == MissionType.DELIVERY_MISSION and base != start_base:
                    new_value_dest = base

    return new_value_start, new_value_dest


@app.callback(
    Output("start_group", "children"),
    Input('select_mission_start', 'value'),
    Input("mission", "value"),
    prevent_initial_call=True
)
def draw_start_base(start_base, mission_value):
    """
    draws a marker for a selected base and a circle showing the maximum range for a mission depending on the type of mission

    Args:
        start_base(str): name of the start base
        mission_value(int): value of the selected mission

    Returns:
        list[dict]: a dictionary for parameters of marker and circle each

    """
    children = []
    mission_type = MissionType(mission_value)
    max_distance = (DEL_MISSION_MAX_DIST if mission_type == MissionType.DELIVERY_MISSION else REC_MISSION_MAX_DIST)
    if start_base is not None:
        children.append(
            dl.Marker(
                id="marker_start",
                position=BASES[start_base][1],
                children=dl.Tooltip("Start: ({:.3f}, {:.3f})".format(*BASES[start_base][1])),
                bubblingMouseEvents=True)
        )
        children.append(
            dl.Circle(
                id="range_circle",
                opacity=1,
                center=BASES[start_base][1],
                radius=max_distance * 1000,
                color='rgb(255,128,0,0.4)',
                children=[dl.Tooltip(f'maximum range: {max_distance}km', direction="bottom", opacity=0.7)]
            ),
        )
    return children


def square_center(bounds):
    return [bounds[0]['lat'] + (bounds[1]['lat'] - bounds[0]['lat']) / 2, bounds[0]['lng'] + (bounds[1]['lng'] - bounds[0]['lng']) / 2]


@app.callback(
    Output("dest_group", "children"),
    Output("edit_control", "editToolbar"),
    Input('select_mission_dest', 'value'),
    Input("edit_control", "geojson"),
    Input("mission", "value"),
    prevent_initial_call=True
)
def draw_dest_base(dest_base, geo, mission_value):
    """
    drawing a marker for the mission destination onto the map. Deletes marker and SAR_squares, if mission type is changed

    Args:
        dest_base(str): name of the destination base
        geo: geojson dictionary containing the components of the edit control
        mission_value(int): value of the selected mission

    Returns:
        dl.Marker: the destination
        dict: parameters for the edit control, resetting it if mission type was changed

    """
    trigger = dash.callback_context.triggered[0]["prop_id"]
    children = []
    mission_type = MissionType(mission_value)

    if trigger == "mission.value":
        return [], dict(action="clear all", mode="remove", n_clicks=0)  # remove features if mission type is changed

    if mission_type == MissionType.DELIVERY_MISSION:
        if dest_base is not None:
            children.append(
                dl.Marker(id="marker_destination",
                          position=BASES[dest_base][1],
                          children=dl.Tooltip("Destination: ({:.3f}, {:.3f})".format(*BASES[dest_base][1])),
                          bubblingMouseEvents=True)
            )
    elif mission_type == MissionType.RECONNAISSANCE_MISSION:
        if not geo:
            raise PreventUpdate

        for i in geo['features']:
            if i['properties']['type'] == 'rectangle':
                pos = square_center(i['properties']['_bounds'])  # rectangle inside the geojson component
                if children:
                    children.pop()  # only add a marker for the last added rectangle
                children.append(
                    dl.Marker(id="marker_destination",
                              position=pos,
                              children=dl.Tooltip("Destination: ({:.3f}, {:.3f})".format(pos[0], pos[1])),
                              bubblingMouseEvents=True)
                )

    return children, dash.no_update


@app.callback(
    Output("Select UAV", "style"),
    Input("start", "n_clicks"),
    State("Select UAV", "value"),
    State("mission", "value"),
    State('start_group', 'children'),
    State('dest_group', 'children'),
    prevent_initial_call=True
)
def mission_start(n_clicks, selected_uavs, mission_value, start_group, dest_group):
    """
    starts a mission inside a thread for each selected UAV if the start button is clicked

    Args:
        n_clicks(int):  n_clicks of the mission start button (value not used, only the event)
        selected_uavs(list[int]): list of selected UAV's
        mission_value(int): value of the selected mission
        start_group: list of dictionaries for each object inside start_group, needed for the start position
        dest_group: list of dictionaries for each object inside dest_group, needed for the destination position

    Returns:
        PreventUpdate: nothing, style is chosen as output because callbacks need one in dash

    """
    start = [0, 0]
    for elem in start_group:
        if elem["props"]["id"] == "marker_start":
            start = elem["props"]["position"]  # position of the start marker
    dest = dest_group[0]["props"]["position"]  # position of the destination marker

    for uav_nr in selected_uavs:
        uav = UAVS[uav_nr - 1]
        if uav.mission_mode != MissionMode.AVAILABLE:
            print(f"UAV {uav_nr} is currently not available")
            continue

        mission_type = MissionType(mission_value)

        print("No. of Mission: ", uav.n_missions, "\t\tMission Type: ", mission_type.name)
        mission_thread = threading.Thread(target=mission_execution, args=(uav, con, mission_type, start, dest))

        mission_thread.start()

    raise PreventUpdate


Outputs = []
for i in range(N_UAVS):
    Outputs.append(Output(f"ovr_health{i + 1}", "children"))
    Outputs.append(Output(f"mission_progress{i + 1}", "value"))
    Outputs.append(Output(f"mission_status{i + 1}", "children"))
    Outputs.append(Output(f"kpis{i + 1}", "figure"))
    Outputs.append(Output(f"ovr_health{i + 1}", "style"))


@app.callback(
    Outputs,
    [Input("progress_interval", "n_intervals")],
    [State(f"collapse{i + 1}", "is_open") for i in range(N_UAVS)]
    + [State(f"mission_status{i + 1}", "children") for i in range(N_UAVS)],
    prevent_initial_call=True
)
def update_data(n_intervals, *args):
    """
    main callback of the dashboard. Updates components for each UAV everytime the overview interval fires
    returns dash.no_update's, if values haven't changed since last interval

    Args:
        n_intervals:  n_intervals of the uav overview update-interval (value not used, only the event)
        *args: list of variable length depending on N_UAVS. Two values for each UAV, first one telling if the collapse is currently opened,
            second one containing the string of the mission status component

    Returns:
        list: List with new values of health_index, mission_progress, mission_mode, health and health_color for each UAV

    """
    output_array = []

    for i, uav in enumerate(UAVS):
        # components aren't updated, if they are 'AVAILABLE' to relieve the callback. Therefore we need to check, if status was set to AVAILABLE inside the last interval
        last_update = args[N_UAVS + i] != "AVAILABLE" and uav.mission_mode == MissionMode.AVAILABLE

        if not (uav.mission_mode != MissionMode.AVAILABLE or last_update):  # no update if the data hasn't changed
            output_array.append(dash.no_update)
            output_array.append(dash.no_update)
            output_array.append(dash.no_update)
            output_array.append(dash.no_update)
            output_array.append(dash.no_update)
            continue

        health_index = uav.health_index()

        fi = go.Figure(go.Bar(  # graph showing health values for motors and battery
            x=[uav.battery_level, uav.pusher_health, *uav.hover_health[::-1]],
            y=['Battery', 'Pusher Motor'] + [f"Hover Motor {i}" for i in range(UAV.N_HOVER_MOTORS, 0, -1)],
            orientation='h'))
        fi.update_layout(title={'text': "Health", 'y': 1, 'x': 0.5, 'xanchor': 'center', 'yanchor': 'top'},
                         width=320, height=200, margin=dict(l=10, r=0, b=0, t=20, pad=2), )

        output_array.append(str(round(health_index * 100, 2)) + "%")  # health_index
        output_array.append(uav.mission_progress)  # mission_progress
        output_array.append(MissionMode(uav.mission_mode).name)  # mission_mode
        output_array.append(fi if args[i] else dash.no_update)  # health

        if health_index > 0.75:  # changing color of mission status depending on the lowest motor health value
            output_array.append({"color": "darkgreen"})
        elif health_index > 0.4:
            output_array.append({"color": "green"})
        elif health_index > 0.20:
            output_array.append({"color": "darkorange"})
        elif health_index > 0.10:
            output_array.append({"color": "red"})
        else:
            output_array.append({"color": "darkred"})

    return output_array


def update_logs(self, i):
    """
    help function for creating a callback to update each UAV's log table. This happens outside the main callback, because
    log updates happen less often.


    Args:
        self: mission status of the UAV (value not used, only the event)
        i: id of the UAV

    Returns:
        list[str]: containing the latest log entries of UAV with id i

    """
    return list(UAVS[i].logs)


for i in range(N_UAVS):
    # calls the help function for each UAV
    app.callback(
        Output(f"tbl{i + 1}", "data"),
        Input(f"mission_status{i + 1}", "children"),
        prevent_initial_call=True
    )(partial(update_logs, i=i))

# -------------- Clientside-Callbacks --------------------------------------------------------------------------------------
"""Callbacks that only use values from inside the dashboard and not rely on data provided by the server are implemented
as clientside callbacks in Javascript. Executing a callback on clientside instead of serverside makes it a lot faster.
"""

app.clientside_callback(
    # hides components for sorting, if the map is selected and the destination base dropdown if SAR mission is selected
    """
    function(mission_type, tab){
        return [{"display": (mission_type == """ + str(MissionType.DELIVERY_MISSION.value) + """? "block": "none")},
           {"display": (tab == "uavs"? "block": "none")}];
    }
    """,
    Output("row_mission_dest", "style"),
    Output("row_sort", "style"),
    Input("mission", "value"),
    Input("tabs", "value")
)

app.clientside_callback(
    # updates the edit control making it possible to draw rectangle if 'destination' and 'SAR Mission' are selected
    """
    function(mission_type, radios){
        if (mission_type == """ + str(MissionType.RECONNAISSANCE_MISSION.value) + """ && radios == 2)
            return {"circle":false, "rectangle":true, "circlemarker":false,
                    "marker":false, "polyline":false, "polygon":false};
        else return {"circle":false, "rectangle":false, "circlemarker":false,
                    "marker":false, "polyline":false, "polygon":false};
    }
    """,
    Output('edit_control', 'draw'),
    Input("mission", "value"),
    Input("start_dest_radio", "value")
)

app.clientside_callback(
    # enables and disables the intervals for map and uav overview depending on which tab is currently open
    """
    function(tab){
        return [tab != "uavs",  tab != "map"];
    }
    """,
    Output("progress_interval", "disabled"),
    Output("map_interval", "disabled"),
    Input("tabs", "value")
)

for i in range(N_UAVS):
    # opens or closes the collapse inside the uav tab if the detail button is clicked
    app.clientside_callback(
        """
        function(n, is_open){
            if(!is_open)
                return ["Less details", true];
            else 
                return ["Details", false];
        }""",
        Output(f"detail{i + 1}", "children"),
        Output(f"collapse{i + 1}", "is_open"),
        [Input(f"detail{i + 1}", "n_clicks")],
        [State(f"collapse{i + 1}", "is_open")],
        prevent_initial_call=True,
    )

app.clientside_callback(
    # disables sort button if no sorting method is currently selected
    """
    function(value){
        return [value == null]
    }
    """,
    [Output("sort", "disabled")],
    Input("sort_uavs", "value"),
    prevent_initial_call=True)

app.clientside_callback(
    # sorting the uav tabs depending on the selected method
    # CAUTION: to make this callback clientside, it takes the information needed out of the dashboard.
    #   changing the way data is presented might stop it from working
    # data is handed down inside dictionaries resulting in expressions like a.props.children[1].props.children[2].props.children,
    # we recommend using console.log() to understand the structure of date
    # check the 'sort_uavs' component for the meaning of 'value'
    """function(n_clicks, children, value, options){
        if(value == 0){
            children.sort(function(a,b) 
                {return parseInt(a.props.key) - parseInt(b.props.key);});
        }
        else if(value == 1){
            children.sort(function(a,b) 
                {return parseInt(b.props.key) - parseInt(a.props.key);});
        }
        else if(value == 2){
            children.sort(function(a,b) 
                {return parseFloat(a.props.children[1].props.children[2].props.children) - parseFloat(b.props.children[1].props.children[2].props.children);
                });
        }
        else if(value == 3){
            children.sort(function(a,b) 
                {return parseFloat(b.props.children[1].props.children[2].props.children) - parseFloat(a.props.children[1].props.children[2].props.children);
                });
        }
        else if(value == 4){
            children.sort(function(a,b) 
                {
                let a_status = a.props.children[3].props.children[2].props.children;
                let b_status = b.props.children[3].props.children[2].props.children
                if(a_status==b_status) return 0;
                else if(a_status == "AVAILABLE"){
                    return 1;
                }
                else if(a_status == "MAINTENANCE"){
                    if(b_status == "AVAILABLE")
                        return -1;
                    else return 1;
                }
                else return -1;
            });
        }
        else if(value == 5){
            children.sort(function(a,b) 
                {
                let a_status = a.props.children[3].props.children[2].props.children;
                let b_status = b.props.children[3].props.children[2].props.children
                if(a_status==b_status) return 0;
                else if(a_status == "AVAILABLE"){
                    return -1;
                }
                else if(a_status == "MAINTENANCE"){
                    if(b_status == "AVAILABLE")
                        return 1;
                    else return -1;
                }
                else return 1;
            });
        }
        for(var i = 0; i<options.length; i++){
            options[i]["disabled"] = value == i && value < 4;
        }

        return [children, null, options];
    }
    """,
    [Output(f"uebersicht", "children"),
     Output("sort_uavs", "value"),
     Output("sort_uavs", "options")],
    [Input("sort", "n_clicks")],
    [State(f"uebersicht", "children"),
     State("sort_uavs", "value"),
     State("sort_uavs", "options")],
    prevent_initial_call=True)

if __name__ == "__main__":
    app.run_server(threaded=True)
