from mission_generator import mission_profile
from uav import UAV
from constants import TIME_SCALE, MISSION_PREP_TIME, BATTERY_CHARGING_TIME
import time
import psycopg2.extensions
from mission_status import *
import math
import geopy.distance

"""time.sleep(max(TIME_SCALE - (time.time() - start_time), 0)) controls the time scale. It pauses the execution, if elapsed time 
since 'start_time' is below TIME_SCALE"""


def mission_execution(uav: UAV, con: psycopg2.extensions.connection, mission_type: MissionType, start, dest) -> None:
    """Simulates the execution of mission for the given UAV

    Args:
        uav(UAV): UAV to simulate mission for
        con(psycopg2.extensions.connection): connection to the database
        mission_type(MissionType): type of mission
        start(list[float]): latitude and longitude of start position
        dest(list[float]): latitude and longitude of destination

    Returns: None

    """
    distance = geopy.distance.great_circle(start, dest).km  # distance between start and destination

    print("Length: \n", distance)
    uav.position = start
    uav.battery_capacity -= UAV.BATTERY_DEGRADATION_FACTOR  # battery capacity degrades each mission
    uav.mission_type = mission_type

    cur = con.cursor()  # cursor for database operations

    # start mission preparation
    uav.mission_mode = MissionMode.MISSION_PREPARATION
    for r in range(MISSION_PREP_TIME):
        start_time = time.time()
        uav.store_to_database(con, cur)
        time.sleep(max(TIME_SCALE - (time.time() - start_time), 0))

    uav.mission_mode = MissionMode(mission_type.value)  # change status: on mission

    print("UAV {} started on mission\n".format(uav.uav_id))

    # generating a mission profile depending on the type of mission
    mission = mission_profile(distance, mission_type)

    # start mission
    uav.set_log(MissionLog.MISSION_START)

    for i in range(len(mission)):
        start_time = time.time()
        uav.degradation(mission[i])  # new health values for single step
        if uav.flight_mode == FlightMode.CRUISE_MODE:
            uav.update_position(dest)  # moving uav while in cruise mode
        uav.mission_progress = (i+1)/len(mission) * 100  # progress of mission in %
        uav.store_to_database(con, cur)
        time.sleep(max(TIME_SCALE - (time.time() - start_time), 0))

        flight_home_time = math.ceil(geopy.distance.distance(uav.position, start).km / UAV.CRUISE_SPEED * 60)  # rounded up time it takes to reach home base in minutes

        # start r2h mission while on sar mission if battery is low or uav is too far away from it's home base
        if (0.1 < uav.battery_level < 0.3 or len(mission) - i < flight_home_time + UAV.LANDING_TIME) and mission_type == MissionType.RECONNAISSANCE_MISSION:
            uav.searching = False
            uav.mission_mode = MissionMode.R2H_MISSION
            uav.mission_type = MissionType.R2H_MISSION

            if 0.1 < uav.battery_level < 0.3:
                uav.set_log(MissionLog.BATTERY_CRITICAL)

            flight_home_distance = geopy.distance.distance(uav.position, start).km
            r2h_mission = mission_profile(flight_home_distance, MissionType.R2H_MISSION)

            # Safe R2H triggered with a length of r2h_distance in km
            uav.set_log(MissionLog.R2H_MISSION)

            for k in range(len(r2h_mission)):
                start_time = time.time()
                uav.degradation(r2h_mission[k])  # new health values for single step
                if uav.flight_mode == FlightMode.CRUISE_MODE:
                    uav.update_position(start)  # moving uav while in cruise mode
                uav.mission_progress = 100 * (k+1)/len(r2h_mission)  # progress of r2h mission in %
                uav.store_to_database(con, cur)
                time.sleep(max(TIME_SCALE - (time.time() - start_time), 0))

            start_time = time.time()
            # Safely landed at base
            uav.set_log(MissionLog.SAFELY_LANDED)
            uav.store_to_database(con, cur)
            time.sleep(max(TIME_SCALE - (time.time() - start_time), 0))

            break

        elif uav.battery_level < 0.1:
            uav.set_log(MissionLog.BATTERY_DEAD)  # Battery dead!

            uav.store_to_database(con, cur)
            break

    else:  # normal mission ending (python for-else construct: this code is executed if the loop isn't interrupted for an alternative mission ending)
        start_time = time.time()
        uav.set_log(MissionLog.MISSION_COMPLETED)  # Mission completed
        uav.store_to_database(con, cur)
        time.sleep(max(TIME_SCALE - (time.time() - start_time), 0))

    uav.mission_mode = MissionMode.MAINTENANCE  # change status: maintenance

    # charging battery
    uav.set_log(MissionLog.BATTERY_CHARGING)

    # load battery before next mission while on ground, respect max cap degradation
    recharging_time = math.ceil((uav.battery_capacity - uav.battery_level) * BATTERY_CHARGING_TIME)  # time in minutes for full recharge considering the current level an max capacity
    print(f"UAV {uav.uav_id} recharging batteries for {recharging_time} minutes\n")
    for q in range(recharging_time):
        start_time = time.time()
        uav.battery_level = min(uav.battery_capacity, uav.battery_level + 1/BATTERY_CHARGING_TIME)
        uav.store_to_database(con, cur)
        time.sleep(max(TIME_SCALE - (time.time() - start_time), 0))

    uav.battery_level = uav.battery_capacity  # if BATTERY_CHARGING_TIME == 0

    start_time = time.time()
    uav.mission_type = MissionType.NONE
    uav.mission_progress = 0  # reset mission progression bar
    uav.mission_mode = MissionMode.AVAILABLE  # change status: available
    uav.set_log(MissionLog.BATTERY_CHARGED)  # Battery charged
    uav.store_to_database(con, cur)
    time.sleep(max(TIME_SCALE - (time.time() - start_time), 0))

    # reset values for spiral search
    uav.n_missions += 1
    uav.searching = False
    uav.angle = 65
    uav.angle_diff = 0
    # close cursor
    cur.close()
