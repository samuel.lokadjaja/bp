from enum import Enum
import numpy

"""This file declares frequently used states and events as enumerations to make it easier to change and comprehend them"""


class MissionLog(Enum):
    """This enumeration class defines different events that can occur during a drone mission"""

    MISSION_START = 0
    BATTERY_CRITICAL = 2
    R2H_MISSION = 3
    SAFELY_LANDED = 4
    BATTERY_CHARGING = 5
    BATTERY_CHARGED = 6
    BATTERY_DEAD = 7
    MISSION_COMPLETED = 8


class MissionMode(Enum):
    """This enumeration class defines the different drone states"""
    
    AVAILABLE = 0
    MISSION_PREPARATION = 1
    DELIVERY_MISSION = 2
    RECONNAISSANCE_MISSION = 3
    R2H_MISSION = 4
    MAINTENANCE = 5


class MissionType(Enum):
    """This enumeration class defines the different drone mission types; more information can be found in the README.md"""
    
    NONE = numpy.nan
    DELIVERY_MISSION = MissionMode.DELIVERY_MISSION.value
    RECONNAISSANCE_MISSION = MissionMode.RECONNAISSANCE_MISSION.value
    R2H_MISSION = MissionMode.R2H_MISSION.value


class FlightMode(Enum):
    """This enumeration class defines the drone flight modes"""
    
    NONE = numpy.nan
    CRUISE_MODE = 0
    HOVER_MODE = 1
